---
layout: markdown_page
title: Product Direction - Growth
canonical_path: "/direction/growth/"
---

## Growth Section Overview

The Growth section at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open core product. Our goal is to accelerate and maximize GitLab user and revenue growth, and while doing that, to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our Growth section consists of 4 product groups (adoption, activation, conversion, expansion) and 1 [product analysis](https://about.gitlab.com/direction/product-analysis/) group. Each product group consists of a cross-functional team of Growth Product Managers, Developers, and UX/Designers, with shared analytics, QA and user research functions, and the product analysis group is focused on empowering the product and growth team with analysis and insights.

### Growth Section's Principles

In essence, we are a cross-functional team of product managers, engineers, designers and data analysts with a unique focus and approach to help GitLab grow. Since GitLab Growth Section is still relatively new, we'd like to share the principles we strive to operate under, which will act as our team vision, and also help the rest of the company understand the best ways to collaborate with us.   

#### Principle 1: The Growth section focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the Growth section focuses on connecting users with that value.  We do so by:
* Driving feature adoption by removing barriers and providing guidance
* Lowering Customer Acquisition Cost (CAC) by maximizing the conversion rate across the user journey
* Increasing Life time value (LTV) by increasing retention & expansion
* Lowering sales/support cost by using product automation to do the work

#### Principle 2: The Growth section sits in the intersection of Sales, Marketing, Customer success & Product

Growth teams are by nature cross-functional as they interact and collaborate closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to complement and maximize these team's work, and ultimately allow customers to get value from our product.

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While the Marketing Team works on creating awareness and generating demand via channels such as webinars, content marketing & paid ads, the Growth team can help lower friction in signup and trial flows to maximize the conversion rate, thus generating higher ROI on marketing spend
* While the Sales Team works on converting larger customers in a high touch manner,the Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make the sales process more efficient
* While the Core product Team works on building features to solve customer problems and pain points, the Growth team can help drive product engagement, create usage habit and promote multi-feature adoption
* While the Customer Success & Support Teams work on solving customers problems and driving product adoption via human interaction, the Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

#### Principle 3: The Growth Section uses a data-driven and systematic approach to drive growth

Growth teams use a highly data & experiment driven approach
* We start from clearly defining what success is and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. Growth model helps us identify key variables that contribute to GitLab's north star metric, and describes the connections between them at a high level.
  We continuously iterate our thinking about GitLab's growth model as we learn more about our business, customers, and product, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing).


```mermaid

graph LR
	A[ARR] --> B[ARR from New Paying Customers]
	A --> C[ARR from Existing Paying Customers]
	B --> D[New customers directly sign up for Paid Plan]
	D --> E[# of Visitors]
	D --> F[Conversion Rate]
	D --> G[Average Contract Value]
	B --> H[New Trial Customers sign up for Paid Plan ARR]
	H --> I[# of Trials]
	H --> J[Conversion Rate]
	H --> K[Average Contract Value]
	B --> L[New/Existing Free Users sign up for Paid Plan ARR]
	L --> M[# of Free User Base]
	L --> N[Conversion Rate]
	L --> O[Average Contract Value]
	C --> P[Existing Paid Customer ARR]
	C --> R[Existing Customer Churned ARR]
	C --> Q[Existing Customer Expansion ARR]
	P --> S[Renewal]
	Q --> T[Seats]
	Q --> U[Upgrades]
	T --> V[# of Seats]
	T --> W[$ per Seat]
	U --> X[# of Upgrades]
	U --> Y[Delta ARR $]
	R --> Z[Cancel]
	R --> AA[Downgrade]
	AA --> AB[# of Downgrades]
	AA --> AC[Delta ARR $]
	subgraph Retention
	S
	Z
	end
	subgraph Expansion
	T
	U
	V
	W
	X
	Y
	AA
	AB
	AC
	end
	subgraph Acquisition
	E
	F
	G
	end
	subgraph Conversion
	I
	J
	K
	M
	N
	O
	end

```

Note that the terms used in growth model are broadly defined, for example, "Conversion Rate" here refers to the percentage of various non-paid customers (new, trial, free) converting to paid customers.

* Then by reviewing the sub-metrics, we try to identify the areas with highest potential for improvement, and pick focus area KPIs for those areas. We typically do so by looking for signals such as a very low conversion rate in an important funnel, metrics that seem to be low comparing with industry benchmarks, or a product flow that caused a lot of customer complaints or support tickets etc.         
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI. We utilize an experiment template to capture the hypothesis all the way to the experiment design, rank all experiment ideas using ICE framework, work through the experiment backlog, and then analyze the experiment results.
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next areas we want to focus on

```mermaid
graph LR
    nsm[North Star Metric]--> grm
    grm[Growth Model] --> fak
    fak[Focus Area KPI] -- Data --> ide
    ide[Ideate] --> pri
    pri[Prioritize] --> bld
    bld[Build] --> alz
    alz[Analyze] --> tst
    tst[Test] --> ide

    style nsm fill:#6b4fbb
    style nsm color:#ffffff
    style grm fill:#6b4fbb
    style grm color:#ffffff
    style fak fill:#fba338
    style fak color:#ffffff
    style ide fill:#fa7035
    style ide color:#ffffff
    style pri fill:#fa7035
    style pri color:#ffffff
    style bld fill:#fa7035
    style bld color:#ffffff
    style alz fill:#fa7035
    style alz color:#ffffff
    style tst fill:#fa7035
    style tst color:#ffffff
```

By following this systematic process, we try to make sure that: 1) We know what matters most; 2) Teams can work independently but their efforts will all contribute to overall growth; 3) We always aim to work on the projects with the highest ROI at any given moment. The ultimate goal is that we are not only uncovering all levers to drive growth, but also tackling them as efficiently as possible.
#### Principle 4: The Growth Section experiments a lot and has a “Win or Learn”  mindset

***"Experiments do not fail, hypotheses are proven wrong"***

Growth teams view any changes as an “experiment”, and we try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful in term of driving desired metrics. Refer to section below for a list of experiments



## Growth Section's Current Focus

### Growth Section OKR

Please refer to [here](https://about.gitlab.com/company/okrs/fy21-q4/) for latest GitLab Company OKR

### Growth KPI
Timeframe: FY21 Q4 ~ TBD (until we believe the marginal improvements we can drive are minimal)
- Adoption Group: [New Group Namespace Create Stage Adoption Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-create-stage-adoption-rate)
- Activation Group: [New Group Namespace Verify Stage Adoption Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-verify-stage-adoption-rate)
- Expansion Group: [New Group Namespace with at least two users added](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added)
- Conversion Group:[New Group Namespace Trial to Paid Conversion Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate)


Please refer to [here](https://about.gitlab.com/handbook/product/performance-indicators/) for a complete set of product KPIs

## Growth Section's Current Focus Area

Starting in FY21 Q4, Growth section will take a more focused approach to work on the highest ROI growth opportunities for the business - and the first focus area we've chosen is the new customer free to paid SaaS conversion rate.

Through analysis of our new customer acquisition funnel, we identified there is reasonable room for improvement in this area. In the past year, Growth section has launched many experiments and improvements in this area, such as new user onboarding issue board experiment, improved check-out experience etc, and we have accumulated insights and learnings to allow us to form high quality hypothesis. One key insight we've learned is that first 90 days is critical to a new customer's onboarding, and if this customer successfully adopts our key features in Create & Verify, invites team members to join, and experiences the GitLab's product value via a trial, the customer's likelihood to convert increases significantly.     

Based on this insight, we will have each of the growth groups to focus on one key actions:
- Create Adoption: Create is the first stage most new customers need to set up, and many new customers are here for SCM. If we can improve the adoption rate of create early on, it sets up the customer for success for adopting more features and converting.
- Verify Adoption: Among all stages, using Verify actually has highest correlation with converting to paid customers, we believe this is the aha moment where new customers see the value of a complete devops platform.
- Invite team members: GitLab is designed for teams, if we can get eligible namespaces to effectively add more team members, they are more likely to see the value of GitLab, and we believe that can drive conversion rate.
- Trial to paid conversion: Trial is one of the most effective ways to get new customers to upgrade, and we think there is room for improvement in terms of trial to paid conversion rate. We want to improve this before driving more trials.

This way, we can launch experiments in these 4 areas that can potentially help a new prospect customer see the value of GitLab quickly, and increase their likelihood of conversion. Because there are opportunities to drive 4 actions in some shared new customer flows and touch points, the 4 growth groups will collaborate closely to make sure we have a shared vision for the new customer journey, and won't create a situation that one customer is pulled too many different directions. We will start with our .com product, and migrate applicable learnings and improvements to our self-managed product.


## Growth Section's Mid-term Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab's Growth Section aims to help the company drive growth while improving efficiency in all fronts. Therefore we've aligned our 3-Year Strategy into the the themes below:

## Maximize new customers conversion

##### Theme 1: Improve trial experience to drive new customer growth

- KPI: trial volumes, trial to paid conversion rate, free to paid conversion rate
- Key focus area: sign up experience, trial experience, onboarding


In order to grow efficiently,  we want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need to reduce payback time to generate cash flow that is essential for weathering storms.

As a collaborator to marketing & sales teams, the role the growth team can play here is to aggressively analyze, test and improve the new user flow and in-product experience between about.gitlab.com, GitLab.com, self-hosted instance, CustomersDot etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signs up for a paid plan.

In order to achieve this goal, we try to understand what the drivers are leading to new customer conversion and amplify them. For example, we have identified through analysis that the number of users in a team, as well the numbers of stages/features a team tries out, all seem to be correlated with a higher conversion rate to a paid plan.

Again, in order to drive this theme, we will also need to understand the end to end new customer journey, and identify the drivers and barriers via a series of research and analytics projects, such as:


1) Map out GitLab new customers journey and understand any potential experience or data gaps between marketing, sales, growth, product teams

2) Post-purchase survey & Email survey to understand the reasons behind why some customers convert, while others don’t

3) Self-hosted new customer user research


##### Theme 2: Build out PQL programs to become a scalable and effective leads source

- KPI: PQL volumes, PQL conversion rate
- Key focus area: free namespaces

###### Overview
The community of free users on GitLab represents an economic moat for our business. However, there is more untapped potential within this community of users. As a free user of GitLab, it can be challenging while using the product to understand if a paid feature exists that could improve their current task. We plan to address this problem via two avenues. First by investing in a unified feature discovery experience in the product where users can learn about paid features and tiers at contextually relevant moments where we will present them with options on how to proceed including things like "upgrade now" "start a trial" and "talk to sales". And second by investing in usage-PQLs where we will look at usage patterns within the product to make recommendations to users to assist in their adoption journey and for qualified users getting them connected with their sales representative.

###### Current state of PQLs today in the GitLab product
Currently, there are two hand-raise PQL moments in GitLab. The first was introduced at the Cape Town Contribute (called an in-app health check at the time). The way it works is if a free self-managed instance is over a particular user count and has usage ping turned on then we display an option in the top right dropdown to “Get a free instance review” (see screenshot below).
<img src="/images/growth/health_check_dropdown.png" width="250">

If a user selects this instance review option they are brought to the following form
<img src="/images/growth/health_check_form.png" width="600">

Between this being implemented during the Cape Town Contribute in 2018 and the first half of 2020 it generated a total of 28 leads, 3 became customers for a total of $93K in IACV. In the fall of 2020, the Growth team increased the rollout of this “instance review” link from previously only appearing if a free instance had 100+ users to displaying if the instance has 5+ users. In the first two months of 2021, the instance review option has generated 26 leads.

There are two core reasons that the cape town PQL moment is underperforming as a PQL lead source.
1. The “Get a free instance review” is not contextually relevant to the instances’ actual usage of GitLab it simply appears if you had over 100 users (now when you have 5+). There’s no clear value to the user as to why they should select this CTA. It’s not clear how this will make them better at their job or why it will improve their experience within GitLab.
2. The form they have to submit is daunting and again doesn’t provide any context on who they will be speaking with or what value they will get from that conversation.

The second hand-raise PQL point is on the SaaS in-app billing page. In 2020 the Growth team added an MVC PQL point into the SaaS product, whereby if an admin navigates to the in-app billing page they now along with the option to upgrade have an option to “Contact sales”. If a user selects “Contact sales” they are brought to the [marketing website form](https://about.gitlab.com/sales/) for contact sales.

<img src="/images/growth/contact_sales_on_in-app_billing_page.png" width="250">

When the growth team first implemented the “Contact sales” option on the billings page it was run as an A/B test. By including the option to “contact sales” we increased the overall engagement with a primary CTA (upgrade now or contact sales) by x% compared to the control. This validated that there were some users interested in speaking to sales that previously did not take action on the page. To date, we’re consistently seeing a 1% click-through-rate on viewing the in-app billing page to clicking “Contact sales”.

Where the experience can be improved:
1. The redirect from a user being in-app to the marketing website is a jarring visual shift and the marketing form leaves a lot of exit points open i.e. the main website header.
2. When we redirect the user to the marketing site we lose access to the information we already know about them causing the form to be longer. If the form was in the product experience we could remove things like first name, last name, email address reducing the form length and increasing the submission rate.
3. We can provide more context in the in-app experience on the value of the particular tier and why it’s worthwhile to connect with a salesperson

###### The future vision for  hand-raise PQLs
Hand-raise PQLs, trials, and upgrades scale linearly with increased awareness and value in the paid tiers. The keyword here is awareness, currently, our free users are mostly unaware of the additional value they could get from the paid tiers as it’s on the user to navigate to our marketing site or support docs to see if we have a paid feature and what tier it’s in.

We recently conducted a survey of Bronze and Premium SaaS customers asking them what features they were aware of, what features they had used, what features they are interested in. By looking at the features users indicated they are very or somewhat interested in divided by the users that have indicated they’ve never used these features we can identify areas of the product that users would find valuable but they haven’t yet discovered them (raw data).

![image](/images/growth/survey_of_feature_awareness.png)

By seeing that paid users on these tiers are unaware of these features they’re interested in we know that we need to increase the visibility for free users so they can perceive the value of the paid tiers. We need to bring this contextually relevant information to the users at the right moment through a consistent product experience and provide them with the opportunity to self-select on how they’d like to proceed “Start a trial” “Upgrade now” “Talk to sales”.

The growth team has built one of these feature discovery moments (seen below) which in the past three months has generated 1,700 “upgrade now” clicks and 650 “start a trial” clicks when combined with close rates and ASPs the estimated ARR is $38K from this one feature discovery moment ([source](https://app.periscopedata.com/app/gitlab/591514/WIP:-Growth---Conversion---Experiment---Security-Upgrade-Point)).

![image](/images/growth/feature_discovery_moment.png)

The vision is to turn this one feature discovery moment into a universal modal that any product team can easily deploy into their area of the product. This allows us to increase awareness of our paid tiers, provide the needed context to the users at the right moments, and reduces the development time for individual teams to implement the experience. This is how we scale from feature discovery moment today to many dozens while still ensuring we’re providing the best possible user experience.

As we scale this feature discovery moment experience throughout the product it’s going to be imperative that the Growth team partner with the data, marketing, and sales teams to monitor all feature discovery moment views, what CTAs (‘start a trial’, ‘upgrade now’, ‘talk to sales’) were selected and their associated close rates and ASPs. This is how we can ensure that we understand what sources are driving the most value for the business while also watching out for underperforming sources that may be able to be removed which ensures we don’t clutter the UI. By looking at the volume of these CTA clicks multiplied by their trialing 90-day close rates and ASPs will allow the product team to report on in-month product-driven revenue.

Over time the growth team will focus on monitoring and improving volume while also working to increase the view to action rate for these feature discovery moment CTAs. How can we make it easier to start a trial from these experiences as close to one-click as possible? How can we reduce the required fields the user needs to fill out to connect with a salesperson? How can we reduce the time to connect with their salesperson, can we embed their assigned sales reps calendar directly within the product? Do we see lower work/close rates for hand-raise PQLs under a certain team size, should those teams only see self-service options? Should larger teams only see hand raise PQL options? By continuing to optimize this experience we can ensure we’re maximizing the value from our free user community while also ensuring they have the best possible product experience.

###### The future vision for usage based PQLs
The ultimate vision is to establish a smart customer interaction system that will trigger the right interaction based on user ‘s product usage behavior to move users to next stage of lifecycle.  We are still in the early stages of these efforts.

What is a usage PQL? When we detect that certain GitLab users take certain actions or show lack of certain actions while using the product, we can use different interactions to guide them to take an action to move forward in the user lifecycle.

To start we are defining three sub-types of usage PQLs, education, in-app recommendations, and sales alerts. We will balance the internal effort (cost) and the potential return to determine the type of information we provide and the method we trigger.

![image](/images/growth/cost_vs_return_matrix.png)

To assist in user adoption and education we will start with usage based product onboarding where if the namespace has not taken important setup actions we'll recommend those actions to them, for example, setup Create, setup Verify, run your first security scan.

As we discover important actions overtime that warrants further investment we will make recommendations to users and teams within the product. For example, if a single user or a small team is running SAST scanning we may recommend to them in-app to start a trial to test out more advanced scanning features.

For high-value actions taken by larger accounts, for example, a large company with many users is actively running SAST scanning we should alert their assigned sales representative about this usage and the more advanced features available in Ultimate.

The first usage-PQL email series has launched to support [free user onboarding](https://gitlab.com/groups/gitlab-org/growth/-/epics/62#note_483748226) whereby we're sending users content recommending the next feature and/or stage they should adopt. The product team has also started brainstorming different potential usage PQLS across the three types in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/300049). This area will be a continued focus for Growth working across departments with Marketing, Sales, and Customer Success as more teams implement usage-based outreach.


## Drive feature adoption and usage

#### Theme 3: Onboard new users/customers effectively to support their use case

- KPI: new namespace stage adoption rate, time to adoption, new namespace SpO
- Key focus area: first mile, continious onboarding, dashboard 

**Personalized stage onboarding**

Because GitLab is a complete DevOps platform, prospect users and customers start to use the product for different reasons. Based on our marketing team's research, there are several [use cases](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/) which are key to our GTM strategy and often are where we land in an account or are significant expand motions.

1. [Usecase: Version Control and Collaboration](/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/) (all project intellectual property - code, design, and more)
1. [Usecase: Continuous Integration](/handbook/marketing/strategic-marketing/usecase-gtm/ci/) (how teams automate and streamline build and test to improve quality and velocity)
1. [Usecase: DevSecOps](/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/) (how teams shift security left and make it relevant throughout the delivery lifecycle)
1. [Usecase: DevOps Platform](/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/) (making adoption of DevOps practices easier and streamlined, eliminating waste)

The data analysis also shows that new namespaces adopts different GitLab stages at different speed, which highglights the opportunity to provide personalized onboarding experience to our new users/customers, with potential different focus on Create, Verify, Secure adoptions.

**Switcher experience**

The other factor that impact GitLab's new user experience significantly is whether they come from an imcument or start from a clean slate. From user interviews, we understand that if a prospect comes from an existing solution, they often need to overcome more hurdles and their "onboarding" experience is quite different from others.  They sometimes need to maintain 2 solutions at the same time, have to reconfigure many integrations, and encounter errors or issues that are not very well covered in typical "new user onboarding". This is a potential direction growth team can explore

**User onboarding vs. Team onboarding**

GitLab is most valuable when used as a team collaboration tool, therefore not only each individual new user needs to learn how to use GitLab and find value, for teams we need to help the entire group to get settled and get to the team "aha moments" too. This will involve identify a team use case early on, make the team invitation flow smooth and intuitive, and show case the features most valuable to teams. 

**Paid customer onboarding**

For paid customers who subscribe to premium or ultimate, we also need to make sure they are guided properly to use the relevant paid features and reach ideal seat utilzation rate early on.  Our Customer Success team has a strong focus in this area, but growth can supplement that effort by building onboarding and nudge into the product flows.  


#### Theme 4: Drive more stage adoption post onboarding

- KPI: Existing namespace SpO, stage cross-adoption rate  
- Key focus area: continious onboarding, in-product contexual upsell moments  

Post initial onboarding,  we still want to guide customers to use multiple features and stages. One hypothesis is that if a customer uses multiple stages and features of GitLab, they are more likely to get value from a single DevOp platform, thus they are more likely to become a long term customer.

Therefore, in FY21, one of the Growth Section's focus areas will be on helping GitLab customers adopt more stages, and we'll observe if that leads to better retention to confirm the causation. ALong with this, we will need to conduct both [quantitative data analysis](https://gitlab.com/gitlab-data/analytics/-/issues/4762) and [quanlitative research](https://gitlab.com/gitlab-org/growth/product/-/issues/1572) to describe the baseline and understand the drivers and barriers behind stage adoption.  

```mermaid
graph LR
    Create --> Plan
    Verify --> Secure
    Create --> Manage
    Create --> Verify
    Verify --> Release
    Verify --> Package
		Verify --> Configure
    Release --> Monitor
    Release --> Protect
```

#### Theme 5: Improve customer retention to build a solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments, it is critical to serve our current customers well and minimize churn. GitLab has a unique advantage with a subscription business model, and our[ gross retention](https://about.gitlab.com/handbook/product/performance-indicators/#gross-retention) is fairly strong, however if you drill down to look at [cohort-based subscription retention](https://docs.google.com/presentation/d/1RF8b0TYIgnPy1bQpwNvLYT_AIIvu2po9V4yWv9pXVAo/edit#slide=id.g83c44e3899_5_0), we still have room for improvement. On the other hand, in tough times, our customers will potentially be facing challenge and uncertainty as well, therefore it will be naturally harder to upsell them with higher tiers or more seats. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability, and can be right there when customers are ready to expand again.

To improve retention, the Growth Section has identified and will be working on the areas such as:


1) Continue to make the customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical customer moments.  
We have done projects such as:
* Clarifying the options between “auto-renewal” and “cancel”
* [Providing clear information on customer licenses and usage](https://gitlab.com/gitlab-org/gitlab/-/issues/118592)
* Automating renewal processes to minimize sales team burden

And we are also working on quarterly co-term billing for self-hosted customers, continuous improvement of CustomersDot user experience, as well as supporting critical pricing and packaging related projects.

To make sure what we do will benefit customer, we have a weekly cross-functional sync with sales, biz ops, support, finance and closely monitor Customer Success Score (billing related) as our OKR.

2)  Deep dive into the customer behavior and identify leading indicators for churn. This will include projects such as:  
* Creating [Customer MVC health score](https://gitlab.com/gitlab-data/analytics/-/issues/3803) and analyze usage based retention curve with help from data analytics team
* Capturing and analyzing [reasons for cancellation](https://app.periscopedata.com/app/gitlab/572742/Auto-Renew-AB-Test-Result?widget=7523649&udv=0)
* Experimenting with different types of “early interventions” to reduce churn



## Drive growth with insights from data and research

##### Theme 6: Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include:

1) Provide knowledge & [documentation](https://docs.google.com/presentation/d/1J0dctOP-xERo2j0WMWcaBuNqTNaaPuJTxLfIaOhNRHk/edit#slide=id.g74d58e3ba0_0_5) of currently available data sources to understand customer behaviors . Also, establish a framework and best practices to enable consistent and compliant data collection

2) Build out a Product usage metrics framework to evaluate how customers engage with certain features. For example, we successfully finished SMAU data collection and dashbaording project for all GitLab product stages in Q1, and will move on to a similar [North Star Metric project](https://gitlab.com/gitlab-org/product-intelligence/-/issues/374) for all GitLab product features in Q2.

3) Build out Customer journey metrics framework to understand how customers flow the GitLab through funnel, including [end to end cross-functional reporting](https://gitlab.com/gitlab-data/analytics/-/issues/4336) spanning marketing, sales and growth.   


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and that success ultimately comes from constant learning & iteration & testing & sharing, as well as breaking the silos of functional teams to drive towards the same goal.



## Growth Insights Knowledge Base

### Dashboards
Below is a list of the dashboards we build and use daily. Many of these dashboards are used not only by us, but also by executives, broader product team, sales and customer success teams.   

|Area |Dashboard/Report | Description |Date|
| ------ | ------ |------ |------ |
|Overall Product| [Product Adoption Dashboard](https://app.periscopedata.com/app/gitlab/771580/Product-Adoption-Dashboard)  | Product KPIs |FY21 Q1 |
|Overall Growth| [New namespace conversion dashboard](https://app.periscopedata.com/app/gitlab/761347/Group-Namespace-Conversion-Metrics)  | Leading indicators of how new namespaces convert to paid customers |FY20 Q4 |
|Overall Product| [SMAU dashboard](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard)  | Stage Monthly Active User trend for both .com and self managed |FY20 |
|Overall Product| [Stage Retention and Adoption Dashboard](https://app.periscopedata.com/app/gitlab/621735/WIP:-SMAU-Retention-and-Adoption) | How popular are the stages among GitLab customers, and how well are they retaining customers |FY20 |
|Activation |[New Customer Acquisition Dashboard](https://app.periscopedata.com/app/gitlab/531526/Acquisition-Dashboard---Last-90-days) |Trend of acquisition of new customers |FY20 |
|Conversion |[New group namespace trial to paid conversion rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate) | How are new new groups that start trials converting to becoming paid customers |FY20 |
|Adoption |[Renewal Dashboard](https://app.periscopedata.com/app/gitlab/505939/Renewals-Dashboard) | Trend on renewal and cancellation of susbscriptions|FY20 |
|Adoption |[Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention) | Retention metrics and trend |FY20 |
|Expansion | [New groups with at least 2 members in their first seven days](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added) | At what rate do new groups have at least 2 members and how can we increase the team adoption rate |FY20 |
|Expansion | [SPAN Deep Dive Report](https://docs.google.com/document/d/1zt3uUcPJW7Y1dMgtIsi4Z1antbPFPy8fp7Fib4Ps_qw/edit?usp=sharing) |How to understand SPAN (Stage Per Average Namespace) and ideas to improve SPAN|FY20 |
|Overall Product| [EoA Monitoring Metrics - Bronze Impact on Group Namespaces](https://app.periscopedata.com/app/gitlab/810873/EoA-Monitoring-Metrics---Bronze-Impact-on-Group-Namespaces) | How did the removal of the Bronze plan impact free-to-paid conversion as well as group namespace stage adoption |FY21 Q1 |

### Insights

|Link |Type| Description |Date |
| ------ | ------ |------ |------ |
|[Why prospects come to GitLab](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit?usp=sharing)|Survey|It shows top reasons propsects come to GitLab and % of win/loss by reasons |FY21 Q1 |
|[Feature Discovery Survey](https://docs.google.com/spreadsheets/d/1fAs9_5_wzZpdVs3d7D1Iqunb-scF5s_7xD9hNWHacNA/edit?usp=sharing) | Survey |Among our paid customers, which features are they aware, interested in and used? |FY21 Q1 |
|[Increase Stages per Organization](https://docs.google.com/presentation/d/1eyClYu8x2nLOhcNTcKx4-Z_IdtO6S2s9KhgImglcNCE/edit?usp=sharing)  | Analysis |How many stages does an average GitLab customer adopt and how to drive adoption |FY21 Q1|
|[What drives free to paid adoption](https://docs.google.com/presentation/d/11Ks8ArlCGzGY0VrSo68RBkbKwWD7pUEc9I9Vu0bIaR4/edit?usp=sharing) | Analysis |What are the drivers that can lead to higher free to paid conversion rate |FY20 Q4|
|[SaaS products new user onboarding research](https://docs.google.com/presentation/d/13BCqrAjY-guNdiRFXNY2ubLLmQdXUpiyaERxdEdirFA/edit#slide=id.g9dbb0f4ccd_0_0)|Research|How best-in-class SaaS products are designing their first mile and continious onboarding experience|FY21 Q1|
|[Why are prospects coming to GitLab (utilizing Command Plan info)](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit#gid=1745528755)| Analysis | What is the distribution of primary use cases across various sales segment and closed/won vs. closed/lost. | 2021-03-01 |
|[Which What's New items are users most intersted in?](https://app.periscopedata.com/app/gitlab/816678/What's-New)| Dashboard | Dashboard that shows engagement with What's New items | ongoing |
|[Why are Saas users cancelling?](https://app.periscopedata.com/app/gitlab/808007/WIP-Cancellation-Reasoning-Dashboard)| Dashboard | Dashboard that shows the self-reported reasons and verbatims that Saas customers are cancelling in CustomersDot | ongoing |  
|[Free to paid conversion rates by integration usage](https://app.periscopedata.com/app/gitlab/756008/Mike-K-Scratchpad?widget=10229400&udv=0) | Dashboard | See the correlation between integration usage and conversion from free to paid | ongoing |







## Growth Experiments Knowledge Base

Here is a list of our currently running and concluded experiments

| Experiment Name | Group | Status |
| ------ | ------ |------ |
|[In Product Email Campaigns](https://gitlab.com/groups/gitlab-org/growth/-/epics/62) | Activation | Active | 
|[Make Upload Options Accessible from Project Details Page](https://gitlab.com/groups/gitlab-org/-/epics/4742) | Adoption | Active | 
|[Check "Initialize repository with a README" by default](https://gitlab.com/groups/gitlab-org/-/epics/4962) | Adoption | Active | 
|[Add CTA for Project Integrations to Drive Deeper Adoption](https://gitlab.com/groups/gitlab-org/-/epics/4772) | Adoption | Active | 
|[Add "New Repo" CTA](https://gitlab.com/groups/gitlab-org/-/epics/4776) | Adoption | Active | 
| [Optional trial during free SaaS signup](https://gitlab.com/groups/gitlab-org/-/epics/4414) | Conversion | Active |
|[Template zero state page VS Simple template](https://gitlab.com/groups/gitlab-org/growth/-/epics/61) | Activation | Active | 
| [SaaS trial onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4803) | Conversion | Active |
| [SaaS trial active trial status countdown](https://gitlab.com/groups/gitlab-org/-/epics/3538) | Conversion | Active |
| [Add optional ‘invite colleagues’ to the first mile (initial required onboarding steps)](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/351) | Expansion | Active |
| [Add invite option to comment area to drive more invite](https://gitlab.com/gitlab-org/gitlab/-/issues/296640) | Expansion | Active |
|[Introduce users to verify via code quality template walkthrough](https://gitlab.com/groups/gitlab-org/growth/-/epics/68) | Activation | scheduled for 13.12 | 
|[Add Ease Score and Interview Recruitment Track to Onboarding Email Series](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/314) | Activation | scheduled for 13.12 | 
|[Collect JTBD info in First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4821) | Adoption | scheduled for 13.12 | 
|[Simplify Group Creation in First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4825) | Adoption | scheduled for 13.12 | 
| [SaaS continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817) | Conversion | scheduled for 13.12 |
| [Invited user email iteration 2](https://gitlab.com/gitlab-org/gitlab/-/issues/296966) | Expansion | scheduled for 13.12 |
| [increase invite acceptance by removing exit CTAs](https://gitlab.com/gitlab-org/gitlab/-/issues/327239) | Expansion | scheduled for 13.12 |
| [Highlight paid feature during active trial](https://gitlab.com/gitlab-org/gitlab/-/issues/273626)  | Conversion | scheduled for 13.12 |
| [Add reassurance (social validation) to the SaaS trial signup page](https://gitlab.com/gitlab-org/gitlab/-/issues/281027) | Conversion | scheduled for 13.12 |
| [For single user trials add invite to left nav](https://gitlab.com/gitlab-org/gitlab/-/issues/224692) | Conversion | scheduled for 13.12 |
| [Trial form: Verify phone number field content](https://gitlab.com/gitlab-org/gitlab/-/issues/326452) | Conversion | Scheduled for 13.12 |
| [Follow Up Experiment- iterate on known fields within trial signup form](https://gitlab.com/gitlab-org/gitlab/-/issues/327039) | Conversion | scheduled for 13.12 |
| [Remind user of trial status when 14 & 3 days remain](https://gitlab.com/groups/gitlab-org/-/epics/3538) | Conversion | scheduled for 14.0 |
|[Empty versatile template for CI](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/292) | Activation | [Concluded](https://gitlab.com/groups/gitlab-org/growth/-/epics/59#test-1-all-namespaces) | 
|[Empty versatile template for CI for <90 days>](https://gitlab.com/groups/gitlab-org/growth/-/epics/59) | Activation | [Concluded](https://gitlab.com/groups/gitlab-org/growth/-/epics/59#test-2-namespaces-90-days-old) | 
|[Update .com paid sign up flow](https://gitlab.com/gitlab-org/growth/product/issues/87)|Activation|Concluded|
|[Pricing page tier presentation test](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/73)|Activation|Concluded|
|[Clarify Auto renew toggle vs. Cancel](https://gitlab.com/gitlab-org/growth/product/-/issues/43) |Adoption |[Concluded](https://gitlab.com/gitlab-org/growth/product/issues/162)|
|[MVC of What's New](https://gitlab.com/gitlab-org/growth/product/-/issues/1487) | Adoption | [Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/255349)| 
|[Display security navigation to upsell new .com signups](https://gitlab.com/gitlab-org/gitlab/-/issues/34910)|Conversion|Concluded|
|[New user onboarding flow](https://gitlab.com/groups/gitlab-org/-/epics/3526)|Conversion|Concluded|
| [Required group trials](https://gitlab.com/groups/gitlab-org/-/epics/4500)  | Conversion | Concluded |
|[.com Trial flow improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/119015)|Conversion|[Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/215881)|
|[MR with no pipeline Prompt](https://gitlab.com/gitlab-org/growth/product/issues/176)|Expansion|[Concluded](https://gitlab.com/gitlab-org/growth/product/-/issues/1588)|
|[Buy CI minutes option on GitLab.com top right dropdown](https://gitlab.com/groups/gitlab-org/growth/-/epics/28)|Expansion|Concluded|
|[Remove known fields from trial sign up form](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/281#note_540419469) | Conversion | Concluded |









## Growth Section Areas of Focus

### 1. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [About.GitLab.com](https://about.gitlab.com/)

### 2. Growth Group Directions

* [Product Analysis Direction](https://about.gitlab.com/direction/product-analysis/)

### 3. What's next: Growth Group Issue Boards


* [Activation board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion)
* [Adoption Board](https://gitlab.com/groups/gitlab-org/-/boards/2112028?&label_name[]=group%3A%3Aadoption)
* [Product Analysis Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20analytics)

## Growth Definitions

It can be confusing to know what we're all talking about when we use different names for the same things. This is where we can clarify and discuss common terms to make communication easier.

- *Sign-up Experience:* Registering to be a user of GitLab. It generally starts on `about.gitlab.com` and ends when the user is registered and can log in to the application. We have free, paid or trial options.
- *Trial Experience:* The experience of selecting and starting a trial, as well as testing/trialing features in GitLab.com or a self-managed instance during the trial. This could be done by net new users of GitLab, or existing users of GitLab who are interesting in exploring paid features.
- *User Onboarding:* A general term that refers to experiences we create to support users during their first 90 days, or any time they engage with a new stage.
  - *Continuous onboarding:* Specifically our onboarding experience that allows users to choose when and if they want to continue onboarding tasks. It starts with simple tasks and builds to more complex ones.
  - *First Mile:* The "happy path" flow a new user goes through when registering for GitLab and landing in the app for the first time. This is a more confined user flow, as we narrow down options so they can experience GitLab without being overwhelmed by all the other features. It starts at `about.gitlab.com` and ideally ends with them using Create features.
  - *Onboarding Emails:* Refers to emails that are sent to Saas and self-managed users intended to drive stage adoption. 
- *Dashboard:* The "homepage" that is shown post-login to users.

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
