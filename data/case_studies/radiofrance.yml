title: Radio France
cover_image: '/images/blogimages/radio-france-cover-image.jpg'
cover_title: |
  Radio France deploys 5x faster with GitLab CI/CD
cover_description: |
  Radio France adopted GitLab CI/CD after success with code management.
twitter_image: '/images/blogimages/radio-france-cover-image.jpg'

twitter_text: "Discover how @radiofrance deployed 5x faster using GitLab for CI/CD."

customer_logo: '/images/case_study_logos/logoradiofrance.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Telecommunications
customer_location: Paris, France
customer_solution: GitLab Starter
customer_employees: 5,000 
customer_overview: |
  Radio France adopted GitLab CI to minimize context switching and accelerate deployments.
customer_challenge: |
  Context switching and configuration issues with Jenkins caused developers at Radio France to look for an all-in-one solution.

key_benefits:
  - |
    Improved efficiency 
  - |
    Faster deployments
  - |
    Year over year cost savings
  - |
    Zero context switching
  - |
    Collaboration efficiency
  - |
    Developer code ownership 

customer_stats:
  - stat: 5x
    label: Deployment speed (w/Jenkins 10/day, now 50/day)
  - stat: 70%   
    label: Annual cost savings on CI/CD 
  - stat: 82%   
    label: Reduction in cycle time (20 second build deployment down from 6 minutes) 


customer_study_content:
  - title: the customer
    subtitle: French public radio broadcaster
    content:
      - |
        Radio France is a French public service radio broadcaster serving seven stations throughout the country. Radio France designs, builds, and operates websites, mobile applications, APIs, podcasts, voice assistant skills, and audio streaming platforms.

  - title: the challenge
    subtitle:  Configuration issues and context switching
    content:
      - |
        Radio France was using GitLab for Git and Jenkins for the production builds, product images, and all the deployments. Developers were constantly switching between GitLab for source code and Jenkins for all the production builds. The teams at Radio France wanted a way to merge the two technologies in order to avoid constant context switching. Ideally, developers wanted a single solution that could be used for code management and deployment.
      - |
        There were also configuration issues with Jenkins. All of the jobs were configured by the CI/CD team, so developers couldn’t make any changes with production deployments. Developers wanted to be able to own their own code without having to wait for permissions from the CI/CD team. 
      - |
        “We wanted developers to manage their own way of deploying to production. Each team, each project, they don't have the same culture. We deploy websites. We deploy APIs. We deploy a lot of things. We don't all have the same challenges,” said Julien Vey, Operational Excellence Manager.
      - |
        According to Vey, Jenkins is a very static workflow. Developers were unable to customize their projects or add new functionalities easily. The new solution they needed would also have to provide easy integration, customizable jobs, and offer new features without an excessive amount of management.


  - blockquote: That was the main goal that we had - to reunify multiple tools into a single one and make it really easy for developers to deploy to production. We were at 10 per day before the migration. Now with GitLab, we do 50 deployments per day in production, which is way more efficient than when we had to switch between GitLab and Jenkins.
    attribution: Julien Vey
    attribution_title: Operational Excellence Manager, Radio France 
   
  - title: the solution
    subtitle: One solution for code management and CI/CD
    content:
      - |
        Since teams were already happily using GitLab for [source code management](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/), they didn’t have to look far for a solution that also provides [CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/). Developers quickly moved everything into a GitLab pipeline in order to start deploying code to production in one place.
      - |
        Radio France has a lot of teams working on a lot of different projects. Each team manages their own set of services, some teams are up to 20 services. Each service is now in its own GitLab project.
      - |
        “We just used the CI/CD part of GitLab because we already had GitLab that just works. We are embedded within the tools so that we don't have to switch to other tools for [deployment]. The main thing that we like about GitLab is working in a single tool,” Vey stated.
          
  - title: the results
    subtitle: Faster deployments, improved collaboration
    content:
      - |
        Radio France’s technology stack includes PHP, React, NodeJS, Svelte, Golang, RabbitMQ, PostgresQL. All their applications are running on a microservice architecture, built on top of Kubernetes. The teams run multiple instances of GitLab runners, which allows them to perform upgrades without interruption. GitLab is running in a virtual machine on AWS, [using Terraform and Ansible to manage it](https://about.gitlab.com/solutions/gitops/). They use Kops to manage Kubernetes clusters on top of AWS. They are able to manage multiple instance groups that match AWS auto scaling groups.
      - |
        GitLab CI has enabled Radio France to add tests to their websites, including lighthouse, UI, and web bottle tests that they previously were unable to implement with Jenkins due to both cost and time management challenges. 
      - |
        Previously, the teams were using two instances of 64G full time with Jenkins runners, which cost approximately $1100 monthly. With GitLab, teams are using between one to eight spot instances, but only for about 10 hours a day at a cost of about $300 a month. “This makes a cost savings of 70% on our CI/CD costs,” Vey added.
      - |
        “The great benefit of GitLab is that before we were going from five or six minutes for just deploying production. Now, when the build is already done with GitLab, I think it only takes 20 seconds to deploy on any environment,” Vey said. This created an 82% reduction in cycle time over the team’s previous deployments using Jenkins.
      - |
        Overall workflow and collaboration has improved since they made the switch from Jenkins, allowing developers to manage the platform less and customize features more than they previously could. “With Jenkins, it was a very static workflow. We couldn't customize it the way we wanted. It was really hard to add new functionalities,” Vey said. “Now with GitLab, we have huge flexibility and we can make the platform available for people to use without thinking a lot, just deploy it and people can use new features.”
      - |
        Each team is responsible for its own pipeline now. Developers can decide if, how, and when they want to deploy to production. Radio France has implemented all the tooling that is required for a developer to make their own choice.
      - |


      - |
        ## Learn more about GitLab solutions
      - |
        [Value Stream Management with GitLab](/solutions/value-stream-management/)
      - |
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
      - |
        [Choose a plan that suits your needs](/pricing/)


