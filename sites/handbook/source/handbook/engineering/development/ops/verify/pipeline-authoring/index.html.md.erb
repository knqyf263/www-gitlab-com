---
layout: handbook-page-toc
title: "Verify:Pipeline Authoring Group"
description: "The GitLab team page for the Pipeline Authoring Group."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/verify/) and the [Category direction of Pipeline Authoring](https://about.gitlab.com/direction/verify/pipeline_authoring/)

## Mission

The Verify:Pipeline Authoring Group is focused on all the functionality with respect to
Pipeline Authoring.

This team maps to [Verify](/handbook/product/categories/#verify-stage) devops stage.

## Performance Indicator

<%= partial("handbook/engineering/development/ops/verify/includes/performance_indicators.erb") %>

### Core domain

- Pipeline configuration: YAML syntax, linter and configuration parser.
- Pipeline creation: process of building and persisting a pipeline including multi-project
  or child pipelines.

## Team Members

The following people are permanent members of the Verify:Pipeline Authoring group:

### Backend
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Continuous Integration & Verify:Pipeline Authoring', role_regexp: /Pipeline Authoring/) %>

### Frontend
<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Pipeline Authoring/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Verify(?!:)|Pipeline Authoring/, direct_manager_role: 'Backend Engineering Manager, Verify:Continuous Integration & Verify:Pipeline Authoring', other_manager_roles: ['Frontend Engineering Manager, Verify']) %>

## Technologies

<%= partial("handbook/engineering/development/ops/verify/includes/technologies.erb") %>

## Useful Links

<%= partial("handbook/engineering/development/ops/verify/includes/useful_links.erb", locals: { group_label: 'group::pipeline_authoring', group_issues_url: 'https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Apipeline+authoring&scope=all', slack_channel: 'g_pipeline-authoring', slack_url: 'https://gitlab.slack.com/archives/C019R5JD44E' }) %>

## How We Work

### Planning

#### Async Issue Refinement

<%= partial("handbook/engineering/development/ops/verify/includes/issue_refinement.erb") %>

#### Weighting Issues

<%= partial("handbook/engineering/development/ops/verify/includes/weighting_issues.erb") %>

### Release Plans

<%= partial("handbook/engineering/development/ops/verify/includes/release_plans.erb") %>

### Workflow

<%= partial("handbook/engineering/development/ops/verify/includes/dev_workflow.erb", locals: { group: 'Pipeline Authoring', board_id: '2019514' }) %>

#### Engineering and design implementation issues

We split up design and engineering implementation into separate issues.
This makes it easier to know the status of an active issue at a glance.
Design issues go through the first few stages of our workflow. 
Once it's ready for dev, we create a new issue for the engineering implementation.
This will be in the `planning breakdown` phase of the workflow until an implementation plan has been added and weighted.

The health status should be enough to know the general status of an issue.
If you need more information on what tasks remain, you can check the implementation plan.
Every engineering implementation issue needs an implementation plan in the description.
It is a list or a table that outlines all the steps required to complete the feature.
The engineer assigned to the issue is responsible for creating this plan and keeping it up to date.
Here is [an example of one an **implementation plan** in an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/296547#implementation-breakdown).

We will start this process in %14.0 and will retro on it at the end of that milestone.

### Spikes

<%= partial("handbook/engineering/development/ops/verify/includes/spikes.erb") %>

### Frontend Architecture Planning

The Pipeline Authoring Group uses a [lightweight architecture planning process](/handbook/engineering/development/ops/verify/pipeline-authoring/frontend-architecture/) for new features and significant refactoring.  

#### Category Labels

The Pipeline Authoring group supports the product marketing categories described below:

| Label                 | |  | | |
| ----------------------| -------| ----|------------| ---|
| `Category:Pipeline Authoring` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3APipeline%20Authoring) | [Direction](https://about.gitlab.com/direction/verify/pipeline_authoring/) | [Documentation](https://docs.gitlab.com/ee/ci/) |
| `Category:Jenkins Importer` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AJenkins%20Importer) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AJenkins%20Importer) | [Direction](https://about.gitlab.com/direction/verify/jenkins_importer/) | |

#### Feature Labels

| Label                 | |  | Description |
| ----------------------| -------| ----|------------|
| `CI persistence` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20persistence) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI%20persistence) | Issues related to Persistence (workspaces, caching). Does not include artifacts, which is its own label |
| `CI rules` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20rules) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI%20rules) | Issues related to CI rules or linting |
| `CI integrations` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20ntegrations) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI%20integrations) | Issues related to supporting different CI targets directly (for example, Java or Mobile). | Pipeline Authoring |
| `bridge pipelines` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bridge%20pipelines) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=bridge%20pipelines) | Issues related to visualizing how pipelines start and depend on each other. Includes visualizations for triggering, cross-project pipelines, and child/parent pipelines. For job execution, please use `~pipeline processing` |
| `pipeline dag` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pipeline%20dag) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=pipeline%20dag) | Issues related to [Directed Acyclic Graphs](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/) visualization only. For job execution, please use `~pipeline processing` |
| `pipeline graphs` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pipeline%20graphs) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=pipeline%20graphs) | Issues related to pipeline graphs and visualization |
| `pipeline authoring` | [Issues](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=pipeline+authoring) | [MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name%5B%5D=pipeline+authoring) | Issues related to authoring the .gitlab-ci.yml file and CI YAML configuration (https://docs.gitlab.com/ee/ci/yaml/) but excludes issues handled by another label such as "CI rules" |  

#### Other Notable Labels

<%= partial("handbook/engineering/development/ops/verify/includes/notable_labels.erb") %>

### Collaboration with UX and Engineering

To create a high-quality product that is functional and useful – Engineering, PM and Product Designer need to work closely together, combine methodologies, and often connect throughout the product development.

Product Designers play a critical role in the product development of user-facing issues. They collaborate with the Engineering and the Product Manager to design the user experience for the features. Once the design solution is proposed, agreed and validated, the Engineering [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) is assigned to implement that design and functionality during the milestone for which the issue is planned.

Product Designer, PM, and Engineering use `workflow::design` to discuss possible complexities, challenges, and uncover blockers around the proposed solution. To avoid blocking reviews later in the product development flow, the Product Designer, PM, and Engineering should work collaboratively throughout the feature design and development process to check-in often, so that the UX approval on merge requests is not required.

#### Inclusive Development

Our process of planning and development relies heavily on overcommunication rather than any approval gates or automated notification mechanisms. We adhere to the proactive mindset and responsibility of everyone involved to make sure every step in the process is as transparent as it can be.

For both planning and building this means direct, cross-functional, and other relevant stakeholders are included early into the process. This makes sure everyone is able to contribute to the best of their capabilities at the right time in the process. This can include, but is not limited to, GitLab objects, Slack, meetings, and daily standups.

Some practical examples of this are:
- When you open up or begin working on an epic, issue, or merge request consider if all stakeholders are aware of this or should be updated. If unsure, error on the side of updating rather than keeping silent.
- When making significant progress make sure this is seen by the relevant stakeholders even if you don't require immediate feedback by mentioning them rather than relying on automated email notifications. Make their involvement explicit.

Note: A good practice when only wanting to inform rather than requesting a direct action from the mentioned stakeholders is to put `FYI` directly following the @mention handle.

#### Breaking down issues for iteration

We suggest using the below steps to reach the best results in the shortest time:

* Once, through user research, we have identified a user need and have generated the big idea to meet that need, Product Designer drives the process of moving that idea into a solution prototype involving PM and Engineering Team early and often.
* We take that prototype through a user testing to validate it is solving the original problem.
* When the solution is validated with users - it is time to break the big solution down into smaller issues involving a whole team (PM, Engineers, Product Designer, QA and Technical Writer).
* Engineers, Product Designer, EM and PM are expected to work closely together to find the most technically feasible and smallest feature set to deliver value to early customers and provide feedback for future product development. Check out [iteration strategies](#iteration-strategies) for help.

We aim to design broadly for an epic or full feature at least one milestone ahead of time and then break the big solution into smaller issues to pick up in the next milestones.
Suppose working one milestone ahead to design the big solution is not possible. In that case, Engineering and Product Designer will define the first most technically feasible and smallest feature set ([MVC](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc)) to satisfy early customers that will be implemented in the same milestone.

#### Reviewing Merge Requests

UX should not be seen as a "gate" for reviewing Merge Requests. To avoid blocking reviews and speed up the response time, Product Designer and the Engineering DRI should work collaboratively throughout the feature development process and check-in often, so the UX approval at the stage of reviewing merge request is not required.

Tips to avoid blocking reviews:
- Product Designers should point Engineers to the needed patterns to ensure product consistency and speed up development time. (example: Pajamas components for Frontend engineers, API documentation for Back-end engineers)
- Whenever Engineering DRI submits a Merge Request that is causing changes to UI or user experience - Engineering DRI should tag the appropriate Product Designer for visibility. However, no UX approval on Merge Requests is expected at the review stage (except when the functionality was not designed by the UX department).
- Use UX debt labels to follow up implementations that are not fully aligned with the UX specification. This should be scheduled within 30 days of the initial feature release.

#### Dealing with Community Merge Requests

For more details on how to contribute to GitLab generally, please see our [documentation](https://docs.gitlab.com/ee/development/contributing/).

#### Aligning on feature development

The Engineering DRI works with the Product Designer throughout the `workflow:in dev` phase to uncover possible problems with the solution early enough that exhibit unexpected behaviour to what was originally agreed upon. If there are changes to be added that weren't agreed upon in the initial issue - a followup issue should be made and the Engineering DRI should work with the Product Manager to schedule that issue in a following milestone. This allows us to focus on [cleanup over signoff](https://about.gitlab.com/handbook/values/#cleanup-over-sign-off), iterate quickly on issues with [a low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame), and still make sure we accomplish what we've agreed upon. We should be careful not to hold off on completing these followup issues so that we don't build up a significant amount of UX debt issues.

If we find that solutions are consistently not matching the agreed upon design, we will hold a retrospective with the DRI, designer, and product manager to discuss where the gaps in communication are so that we can improve. It may be necessary to begin requiring a UX approval for merge requests on certain issues to help the Engineering DRI meet the requirements.

#### Avoiding crunch times between UX and Engineering

* Ideally, the Product Manager works ahead of time with a Product Designer to validate the problem and work on the solution. See [Validation track](/handbook/product-development-flow/#validation-track) for more details. This allows us to come up with the bigger idea ahead of time, and work further with Engineering to break it down into smaller iterations. Ideally, this should be completed before the implementation milestone starts.
* Product Designer, PM, and Engineering use the [Design phase](/handbook/product-development-flow/#validation-phase-3-design) in the Validation track to talk about complexities and discuss challenges and uncover blockers. Once we are all in agreement - we can have a put a direction on the board - see [Solution Validation phase](/handbook/product-development-flow/#validation-phase-4-solution-validation) for details.
* Engineers and Product Designers should stay in contact and frequently align throughout the [Build track](https://about.gitlab.com/handbook/product-development-flow/#build-track) to avoid unplanned changes.

#### Iteration on UX and Engineering collaboration process

In order to develop and maintain the optimal process for collaboration between UX and Engineering, the Product Designer runs a quarterly UX Collaboration Survey to assess the quality of UX collaboration on the team and gather feedback from the engineers, engineering managers and the product manager.

The results of the first UX collaboration survey can be found in the [UX Collaboration Survey issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/38). We are making iterative process changes based on the results of this survey and sync retrospective discussions, and will reassess the quality of our UX collaboration at the end of FY22-Q2.

### Technical Debt

<%= partial("handbook/engineering/development/ops/verify/includes/technical_debt.erb", locals: { group: 'Pipeline Authoring', board_id: '2019541' }) %>

### Retrospectives

<%= partial("handbook/engineering/development/ops/verify/includes/retrospectives.erb") %>

### Team Communication

<%= partial("handbook/engineering/development/ops/verify/includes/team_communication.erb", locals: { slack_channel: 'g_pipeline-authoring', slack_url: 'https://gitlab.slack.com/archives/CPCJ8CCCX', group_label: 'group::pipeline authoring', group_issues_url: 'https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Apipeline+authoring&scope=all', gitlab_subgroup: 'pipeline-authoring-group' }) %>

### Feature channels
We use [feature channels](https://about.gitlab.com/handbook/communication/chat/#feature-channels-f_) (e.g. #f_awesome_feature) for our larger features.
These slack channels are for internal discussions on ideation, requests for general suggestions, and context. For any product related updates, the Product Manager is responsible for documenting summaries from Slack discussions directly in the issue. For any other discussions relating to the technical details, the Engineering DRI is responsible for documenting these summaries from relevant Slack discussions in the issue and/or making changes in the appropriate merge request.

Discussions about definition and implementation should still happen in merge requests or issues.  By encapsulating summaries of any discussions outside of these documents (e.g. Slack channels/threads or sync calls), we ensure we keep a single source of truth driving our decisions about an implementation.
This ensures the information persists and is visible to a wider audience.

## Developer Onboarding

<%= partial("handbook/engineering/development/ops/verify/includes/developer_onboarding.erb") %>

## Team Resources

See [dedicated page](/handbook/engineering/development/ops/verify/pipeline-authoring/team-resources/).

See also [lightweight architecture planning process](/handbook/engineering/development/ops/verify/pipeline-authoring/frontend-architecture/).
